plugins {
    id 'java-library'
    id 'signing'
    id 'maven-publish'
    id 'jacoco'
    id('io.github.gradle-nexus.publish-plugin') version '1.1.0'
}

apply plugin: 'java'

group = 'network.casper'
version='0.3.14'
sourceCompatibility = 1.8
targetCompatibility = 1.8

repositories {
    mavenCentral()
    maven { url 'https://s01.oss.sonatype.org/content/repositories/snapshots' }
}


dependencies {

    implementation "com.rfksystems:blake2b:${blake2bVersion}"
    implementation "commons-codec:commons-codec:${commonsCodecVersion}"
    implementation "com.fasterxml.jackson.core:jackson-core:${jacksonVersion}"
    implementation "com.fasterxml.jackson.core:jackson-databind:${jacksonDatabindVersion}"
    implementation "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:${jacksonVersion}"
    implementation "com.sshtools:maverick-synergy:${maverickSynergyVersion}"
    implementation "org.apache.commons:commons-lang3:${commonsLangVersion}"
    implementation "com.squareup.okhttp3:okhttp:${okHttpVersion}"
    implementation "commons-io:commons-io:${commonsIoVersion}"
    implementation "com.google.code.findbugs:jsr305:${jsr305Version}"
    implementation "org.web3j:crypto:${web3jCryptoVersion}"
    implementation "org.bouncycastle:bcprov-jdk15on:${bouncycastleVersion}"
    implementation "org.bouncycastle:bcpkix-jdk15on:${bouncycastleVersion}"

    testImplementation "com.squareup.okhttp3:mockwebserver:${mockwebserverVersion}"
    testImplementation "com.jayway.jsonpath:json-path-assert:${jsonPathAssertVersion}"
    testImplementation "org.junit.jupiter:junit-jupiter-api:${junitVersion}"
    testImplementation "org.junit.jupiter:junit-jupiter-params:${junitVersion}"
    testRuntimeOnly "org.junit.jupiter:junit-jupiter-engine:${junitVersion}"
}

java {
    withJavadocJar()
    withSourcesJar()
}

nexusPublishing {
    repositories {
        sonatype {  //only for users registered in Sonatype after 24 Feb 2021
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(System.getenv('MAVEN_USERNAME'))
            password.set(System.getenv('MAVEN_PASSWORD'))
        }
    }
}

javadoc {
    if (JavaVersion.current().isJava9Compatible()) {
        options.addBooleanOption('html5', true)
    }
}

jar {
    manifest {
        attributes(
                'Main-Class': 'com.casper.sdk.CasperSdk'
        )
    }
}

task casperJar(type: Jar) {
    archiveBaseName = 'casper-java-sdk'
    archiveVersion = '0.3.13'
    manifest {
        attributes 'Main-Class': 'com.casper.sdk.CasperSdk'
    }
    from { configurations.compileClasspath.findAll { it.isDirectory() ? it : zipTree(it) } }
    with jar
}

test {
    useJUnitPlatform()
}

publishing {
    repositories {
        maven {
            name = "GitHubPackages"
            url = "https://maven.pkg.github.com/casper-network/casper-java-sdk"
            credentials {
                username = System.getenv("GITHUB_ACTOR")
                password = System.getenv("GITHUB_TOKEN")
            }
        }
    }

    publications {
        mavenJava(MavenPublication) {
            artifactId = 'casper-java-sdk'
            from components.java
            pom {
                name = 'Casper Java SDK'
                packaging = 'jar'
                description = 'SDK to streamline the 3rd party Java client integration processes. Such 3rd parties include exchanges & app developers.'
                url = 'https://github.com/casper-network/casper-java-sdk'

                scm {
                    connection = 'scm:git:https://github.com/casper-network/casper-java-sdk.git'
                    developerConnection = 'git@github.com:casper-network/casper-java-sdk.git'
                    url = 'https://github.com/casper-network/casper-java-sdk'
                }

                issueManagement {
                    system = 'GitHub'
                    url = 'https://github.com/casper-network/casper-java-sdk/issues'
                }

                ciManagement {
                    system = 'Github Actions'
                    url = 'https://github.com/casper-network/casper-java-sdk/actions'
                }

                licenses {
                    license {
                        name = 'The Apache License, Version 2.0'
                        url = 'http://www.apache.org/licenses/LICENSE-2.0.txt'
                    }
                }

                developers {
                    developer {
                        id = 'meywood'
                        name = 'Ian Mills'
                        email = 'ian@meywood.com'
                    }
                    developer {
                        id = 'stormeye'
                        name = 'Carl Norburn'
                        email = 'carl@stormeye.co.uk'
                    }
                }
            }
        }
    }

    jar {
        archiveClassifier = ''
    }

    // Reference at https://docs.gradle.org/current/userguide/signing_plugin.html#sec:in-memory-keys
    signing {
        def signingKey = System.getenv('GPG_SIGNING_KEY') ?: findProperty('GPG_SIGNING_KEY')
        def signingKeyPassword = System.getenv('GPG_SIGNING_KEY_PASSWORD') ?: findProperty('GPG_SIGNING_KEY_PASSWORD')

        useInMemoryPgpKeys(signingKey, signingKeyPassword)
        sign publishing.publications.mavenJava
    }

}
